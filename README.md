# ** DEPRECATED **

**hosted on gitlab**

Evolve an OCI structure using Terraform

## To Do
- [ ] Use [SecurityToken](https://docs.oracle.com/en-us/iaas/Content/API/SDKDocs/terraformproviderconfiguration.htm#securityTokenAuth) for back end instead of `http` which obviates the need for a PAR   
- [ ] Use [InstancePrinciple](https://docs.oracle.com/en-us/iaas/Content/API/SDKDocs/terraformproviderconfiguration.htm#instancePrincipalAuth) for back end instead of `SecurityToken`  
    - [ ] Set up IaC/Dev VM
    - [ ] Generate token
- [ ] Utilise TerraGrunt
- [ ] CI/CD Pipeline
- [ ] Understand importing existing resources to bring under TF control
