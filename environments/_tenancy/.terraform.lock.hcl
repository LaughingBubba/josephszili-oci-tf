# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/oci" {
  version     = "5.5.0"
  constraints = ">= 5.5.0"
  hashes = [
    "h1:iZJvQZKEmabavvx05U2TPAFc0Bc/CXxjOP7b0qlYKH0=",
  ]
}
