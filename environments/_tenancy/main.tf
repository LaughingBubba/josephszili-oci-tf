terraform {
  required_providers {
    oci = {
      source = "hashicorp/oci"
      version = ">= 5.5.0"
    }
  }
  backend "http" {
    update_method = "PUT"
    address       = "https://objectstorage.ap-melbourne-1.oraclecloud.com/p/O3q1A2yn8adDwoZ4_0szdPYfiapiaYG37it3U2YLX-aLlIuSFUomhzpqeHDciyNF/n/ax6kuvnaadqr/b/oci-terraform-state/o/terraform.tfstate"
  }
}
/* Bucket & Pre-Authenticatd Request (par) for TF
Name:   par-terraform-expirydate-expirytime
        par-terraform-20231231-1224
URL:    https://<<pre-authenticated-url>>
Upload pre-existing when converting to remote backend for the 1st time: 
 curl https://<<pre-authenticated-url>> --upload-file terraform.tfstate
*/

// This should be created out of band so state never references it. This is here as FYI
resource "oci_objectstorage_bucket" "oci-terraform-state" {
    #Required
    compartment_id = var.tenancy_ocid
    name = "oci-terraform-state"
    namespace = var.namespace

    # Optional
    auto_tiering = "InfrequentAccess"
    freeform_tags = {"env"= "tenancy", "retain"= "true"}
    object_events_enabled = true
    versioning = "Enabled"
}