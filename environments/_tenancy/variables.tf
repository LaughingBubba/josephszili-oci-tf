/********************/
# Tenancy Related Vars
/********************/
variable "tenancy_ocid" {
    description = "OCID of the parent compartment"
    type        = string
}
variable "tenancy_region" {
    # Required
    description = "home region"
    type        = string
}
variable "namespace" {
    # Required
    description = "backet namespace for the tanancy"
    type        = string
}
/************************/
# Environment Related Vars
/************************/
variable "env" {
    # Required
    description = "Environment short code"
    type        = string
}
variable "env_description" {
    # Required
    description = "Environment description - one liner"
    type        = string
}
variable "instance_type" {
    # Required
    description = "compute instance type"
    type        = string
}
/************************/
# Tags
/************************/
