#----------------------------------------#
# DEV - DEVELOPMENT ENVIRONMENT DEFINITION
#----------------------------------------#
terraform {
  required_providers {
    oci = {
      source = "hashicorp/oci"
      version = ">= 5.5.0"
    }
  }
  backend "http" {
    update_method = "PUT"
    address       = "https://objectstorage.ap-melbourne-1.oraclecloud.com/p/O3q1A2yn8adDwoZ4_0szdPYfiapiaYG37it3U2YLX-aLlIuSFUomhzpqeHDciyNF/n/ax6kuvnaadqr/b/oci-terraform-state/o/vms/terraform.tfstate"
  }
}
provider "oci" {
  tenancy_ocid     = var.tenancy_ocid
  region           = var.tenancy_region
  user_ocid        = "ocid1.user.oc1..aaaaaaaarjekitgsyvevthoz56xbjomm6psflvqhbr6sxjtv35plgi2vezbq"
  private_key_path = "~/.oci/jszili.pem"
  fingerprint      = "b1:f4:b8:19:44:a2:70:cc:ac:09:e3:41:38:1c:4e:0b"
}

##################
# COMPARTMENT
# NOTE: Only Compartments with {enable_delete = true} are eligible for destruction
module "vms_cmp" {
    source = "../../modules/identity/compartment"
    # input variables
    parent_compartment  = var.tenancy_ocid
    description         = "VM Hosting Compartment"
    name                = "vms-cmp"
    is_deleteable       = true
    # Optional
    # freeform_tags       = {cost_centre="BTS"}
}