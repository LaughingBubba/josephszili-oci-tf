#----------------------------------------#
# PRD - PRODUCTION ENVIRONMENT DEFINITION
#----------------------------------------#
terraform {
  required_providers {
    oci = {
      source = "hashicorp/oci"
    }
  }
  backend "http" {
    update_method = "PUT"
    address       = "https://objectstorage.ap-melbourne-1.oraclecloud.com/p/O3q1A2yn8adDwoZ4_0szdPYfiapiaYG37it3U2YLX-aLlIuSFUomhzpqeHDciyNF/n/ax6kuvnaadqr/b/oci-terraform-state/o/prd/terraform.tfstate"
  }
}
provider "oci" {
  tenancy_ocid     = var.tenancy_ocid
  region           = var.tenancy_region
  user_ocid        = "ocid1.user.oc1..aaaaaaaarjekitgsyvevthoz56xbjomm6psflvqhbr6sxjtv35plgi2vezbq"
  private_key_path = "~/.oci/jszili.pem"
  fingerprint      = "b1:f4:b8:19:44:a2:70:cc:ac:09:e3:41:38:1c:4e:0b"
}

/*--------------------*/
# PRD - IDENTITY DOMAIN
/*--------------------*/
# NOTE: IDD's need to be "deactivated" before being eligible for destruction
module "prd_idd" {
    source = "../../modules/identity/domain"
    # input variables
    compartment_id  = var.tenancy_ocid
    region          = var.tenancy_region
    description     = "PRD env - Production Identity Domain"
    name            = "prd-idd"
    license_type    = "free"
}

##################
# COMPARTMENT
# NOTE: Only Compartments with {enable_delete = true} are eligible for destruction
module "prd_cmp" {
    source = "../../modules/identity/compartment"
    # input variables
    parent_compartment  = var.tenancy_ocid
    description         = "PRD env - Production Compartment"
    name                = "prd-cmp"
    is_deleteable       = true
}

########################
# USER GROUPS & POLICIES
# https://registry.terraform.io/providers/oracle/oci/latest/docs/resources/identity_group

# https://registry.terraform.io/providers/oracle/oci/latest/docs/resources/identity_domains_group
# https://docs.oracle.com/en-us/iaas/tools/oci-cli/3.29.4/oci_cli_docs/cmdref/identity-domains/group/delete.html


# https://registry.terraform.io/providers/oracle/oci/latest/docs/resources/identity_policy
########################

/*-------------------------------------------*/
# Admins
/*-------------------------------------------*/
resource "oci_identity_domains_group" "prd-ug-admin" {
    # Required
    display_name = "prd-ug-admin"
    idcs_endpoint = module.prd_idd.env_url
    schemas = [
        "urn:ietf:params:scim:schemas:core:2.0:Group",
        "urn:ietf:params:scim:schemas:oracle:idcs:extension:OCITags"
    ]
}

resource "oci_identity_policy" "prd-pg-admin" {
    # Required
    compartment_id  = module.prd_cmp.env_compartment
    description = "PRD env - Admins policy"
    name = "prd-pg-admin"
    statements = [
        "allow group prd-idd/prd-ug-admin to manage all-resources in compartment ${module.prd_cmp.env_compartment_name}"
    ]
}

/*-------------------------------------------*/
# Lead Developers Group & Policy
/*-------------------------------------------*/
resource "oci_identity_domains_group" "prd-ug-dev-lead" {
    # Required
    display_name = "prd-ug-dev-lead"
    idcs_endpoint = module.prd_idd.env_url
    schemas = [
        "urn:ietf:params:scim:schemas:core:2.0:Group",
        "urn:ietf:params:scim:schemas:oracle:idcs:extension:OCITags"
    ]       
}

resource "oci_identity_policy" "prd-pg-dev-lead" {
    compartment_id  = module.prd_cmp.env_compartment
    description = "PRD env - Lead Developers policy"
    name = "prd-pg-dev-lead"
    statements = [
        "allow group prd-idd/prd-pg-dev-lead to inspect all-resources in compartment ${module.prd_cmp.env_compartment_name}"
    ] 
}