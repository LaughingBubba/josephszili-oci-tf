#----------------------------------------#
# DEV - DEVELOPMENT ENVIRONMENT DEFINITION
#----------------------------------------#
terraform {
  required_providers {
    oci = {
      source = "hashicorp/oci"
      version = ">= 5.5.0"
    }
  }
  backend "http" {
    update_method = "PUT"
    address       = "https://objectstorage.ap-melbourne-1.oraclecloud.com/p/O3q1A2yn8adDwoZ4_0szdPYfiapiaYG37it3U2YLX-aLlIuSFUomhzpqeHDciyNF/n/ax6kuvnaadqr/b/oci-terraform-state/o/dev/terraform.tfstate"
  }
}

/*--------------------*/
# DEV - IDENTITY DOMAIN
/*--------------------*/
# NOTE: IDD's need to be "deactivated" before being eligible for destruction
module "dev_idd" {
    source = "../../modules/identity/domain"
    # input variables
    compartment_id  = var.tenancy_ocid
    region          = var.tenancy_region
    description     = "DEV env - Developement Identity Domain"
    name            = "dev-idd"
    license_type    = "free"
}

##################
# COMPARTMENT
# NOTE: Only Compartments with {enable_delete = true} are eligible for destruction
module "dev_cmp" {
    source = "../../modules/identity/compartment"
    # input variables
    parent_compartment  = var.tenancy_ocid
    description         = "DEV env - Development Compartment"
    name                = "dev-cmp"
    is_deleteable       = true
    # Optional
    # freeform_tags       = {cost_centre="BTS"}
}

########################
# USER GROUPS & POLICIES
# https://registry.terraform.io/providers/oracle/oci/latest/docs/resources/identity_group

# https://registry.terraform.io/providers/oracle/oci/latest/docs/resources/identity_domains_group
# https://docs.oracle.com/en-us/iaas/tools/oci-cli/3.29.4/oci_cli_docs/cmdref/identity-domains/group/delete.html


# https://registry.terraform.io/providers/oracle/oci/latest/docs/resources/identity_policy
########################

/*-------------------------------------------*/
# Admins
/*-------------------------------------------*/
resource "oci_identity_domains_group" "dev-ug-admin" {
    # Required
    display_name = "dev-ug-admin"
    idcs_endpoint = module.dev_idd.env_url
    schemas = [
        "urn:ietf:params:scim:schemas:core:2.0:Group",
        "urn:ietf:params:scim:schemas:oracle:idcs:extension:OCITags"
    ]
}

resource "oci_identity_policy" "dev-pg-admin" {
    # Required
    compartment_id  = module.dev_cmp.env_compartment
    description = "DEV env - Admins policy"
    name = "dev-pg-admin"
    statements = [
        "allow group dev-idd/dev-ug-admin to manage all-resources in compartment ${module.dev_cmp.env_compartment_name}"
    ]
}

/*-------------------------------------------*/
# Lead Developers Group & Policy
/*-------------------------------------------*/
resource "oci_identity_domains_group" "dev-ug-dev-lead" {
    # Required
    display_name = "dev-ug-dev-lead"
    idcs_endpoint = module.dev_idd.env_url
    schemas = [
        "urn:ietf:params:scim:schemas:core:2.0:Group",
        "urn:ietf:params:scim:schemas:oracle:idcs:extension:OCITags"
    ]       
}

resource "oci_identity_policy" "dev-pg-dev-lead" {
    compartment_id  = module.dev_cmp.env_compartment
    description = "DEV env - Lead Developers policy"
    name = "dev-pg-dev-lead"
    statements = [
        "allow group dev-idd/dev-pg-dev-lead to manage all-resources in compartment ${module.dev_cmp.env_compartment_name}"
    ] 
}

/*-------------------------------------------*/
# Developers Group & Policy
/*-------------------------------------------*/
resource "oci_identity_domains_group" "dev-ug-dev" {
    # Required
    display_name = "dev-ug-dev"
    idcs_endpoint = module.dev_idd.env_url
    schemas = [
        "urn:ietf:params:scim:schemas:core:2.0:Group",
        "urn:ietf:params:scim:schemas:oracle:idcs:extension:OCITags"
    ]      
}

resource "oci_identity_policy" "dev-pg-dev" {
    compartment_id  = module.dev_cmp.env_compartment
    description = "DEV env - Developers policy"
    name = "dev-pg-dev"
    statements = [
        "allow group dev-idd/dev-pg-dev to use all-resources in compartment ${module.dev_cmp.env_compartment_name}"
    ] 
}