##################
# COMPARTMENT
# https://registry.terraform.io/providers/oracle/oci/latest/docs/resources/identity_compartment
# NOTE: Only Compartments with {enable_delete = true} are eligible for destruction

resource "oci_identity_compartment" "cmp" {
  # Required
  compartment_id = var.parent_compartment
  description    = var.description
  name           = var.name
  enable_delete  = var.is_deleteable 

  # Optional
  # defined_tags = {"Operations.CostCenter"= "42"}
  # freeform_tags = var.freeform_tags
}