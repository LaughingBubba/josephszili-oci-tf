output "env_compartment" {
    value = oci_identity_compartment.cmp.id
}

output "env_compartment_name" {
    value = oci_identity_compartment.cmp.name
}