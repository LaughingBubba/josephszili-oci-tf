# https://registry.terraform.io/providers/oracle/oci/latest/docs/resources/identity_group
resource "oci_identity_group" "ug" {
    # Required
    compartment_id = var.compartment_id
    description = var.description
    name = var.name

    # Optional
    # defined_tags = {"Operations.CostCenter"= "42"}
    # freeform_tags = {"Department"= "Finance"}
}